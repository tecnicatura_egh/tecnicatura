package tp2.entidades;

public class Usuario {
	private String username;
	private String password;
	private String nombre;
	private String email;
	
	public Usuario(String username, String password, String nombre, String email) { //constructor
		this.username = username;
		this.password = password;
		this.nombre = nombre;
		this.email = email;
		
	}
	//GETTERS AND SETTERS
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	
}
