package tp2.servicios;

import java.util.ResourceBundle;

import tp2.entidades.Usuario;

public class UsuarioServicio {
	
	public Usuario login(String nombreUsuario, String contrasenya) {
		ResourceBundle rb = ResourceBundle.getBundle("credenciales");
		String username = rb.getString("username");
		String password = rb.getString("password");
		String nombre = rb.getString("nombre");
		String email = rb.getString("email");
		
		if(username.equals(nombreUsuario) && password.equals(contrasenya)) {
			
			return new Usuario(username, password, nombre, email);
			
		}else {
			
			return null;
		}
	}

}
